import {useState, useEffect,useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate,useNavigate,Link,useParams} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function UpdateProduct() {
   
    const {user, setUser} = useContext(UserContext)
    const { bookId } = useParams();


    const [books, setBooks] = useState([]);


 

    // to store and manage value of the input fields
    const [title, setTitle] = useState(books.title);
    const [genre, setGenre] = useState(books.genre);
    const [author, setAuthor] = useState(books.author);
    const [description, setDescription] = useState(books.description);
    const [price, setPrice] = useState(books.price);
    const [stock, setStock] = useState(books.stock);
    const [tags, setTags] = useState([]);

    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate('')

       useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setBooks(data);
        })
    }, [])

    



function update(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/update`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
        body: JSON.stringify({
            title: title,
            genre: genre,
            author: author,
            description: description,
            price: price,
            stock: stock,
            tags: tags,
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)
        if(data) {
          
            Swal.fire({
                    title: "Book successfully updated",
                    icon: "success",
                    text: `Add more!`
                })
            navigate("/")
        } else if (data.userNotAdmin) {
            alert("User is not admin")
        }
    });
}


    return (
         (user.isAdmin === false)? 
        <Navigate to="/courses"/>
         :
    <Form onSubmit={(e) => update(e)}>
      
      <Form.Group controlId="title">
        <Form.Label>Title</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book title"
          defaultValue={books.title}
          onChange={(e) => setTitle(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="genre">
        <Form.Label>Genre</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book genre"
          defaultValue={books.genre}
          onChange={(e) => setGenre(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="author">
        <Form.Label> Author</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book author"
          defaultValue={books.author}
          onChange={(e) => setAuthor(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter description"
          defaultValue={books.description}
          onChange={(e) => setDescription(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter price"
          defaultValue={books.price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="stock">
        <Form.Label>Stock</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter stock"
          defaultValue={books.stock}
          onChange={(e) => setStock(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="tags">
        <Form.Label>Tags</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter tags separated by comma"
          defaultValue={books.tags}
          onChange={(e) => setTags(e.target.value.split(','))}
          required
        />
      </Form.Group>

      

        <Button variant="primary" type="submit" >
          Update
        </Button>
      </Form>
      );
  


}
