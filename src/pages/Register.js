import {useState, useEffect,useContext} from 'react';
import locationData from '../data/locationData';
import { Form, Button, Container , Row , Col , Image, Navbar , NavDropdown, Nav} from 'react-bootstrap';
import {Navigate,useNavigate,Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';
import mylogo from "../images/mylogo.png"

export default function Register() {
   
    const {user, setUser} = useContext(UserContext)

    // to store and manage value of the input fields
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [name, setName] = useState('');
    const [username, setUsername] = useState('');
    const [region, setRegion] = useState('');
    const [province, setProvince] = useState('');
    const [municipality, setMunicipality] = useState('');
    const [barangay, setBarangay] = useState('');
    const [completeAddress, setCompleteAddress] = useState('');
    const [municipalityOptions, setMunicipalityOptions] = useState([]);
    const [provinceOptions, setProvinceOptions] = useState([]);
    const [barangayOptions, setBarangayOptions] = useState([]);
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate('')

    const regionOptions = Object.entries(locationData).map(([regionCode, regionData]) => (
    <option key={regionCode} value={regionCode}>{regionData.region_name}</option>
    ));

     const onRegionChange = (e) => {
    const regionValue = e.target.value;
    setRegion(regionValue);
    setProvince('');
    setMunicipality('');
    setBarangay('');
  }

     useEffect(() => {
    if (region === '') {
      setProvinceOptions([]);
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const provinces = Object.entries(locationData[region].province_list).map(([provinceName, provinceData]) => (
        <option key={provinceName} value={provinceName}>{provinceName}</option>
      ));
      setProvinceOptions(provinces);
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    }
  }, [region]);

    const onProvinceChange = (e) => {
    const provinceValue = e.target.value;
    setProvince(provinceValue);
    setMunicipality('');
    setBarangay('');
  }

    useEffect(() => {
    if (province === '') {
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const municipalities = Object.entries(locationData[region].province_list[province].municipality_list).map(([municipalityName, municipalityData]) => (
        <option key={municipalityName} value={municipalityName}>{municipalityName}</option>
      ));
      setMunicipalityOptions(municipalities);
      setBarangayOptions([]);
      setIsActive(false);
    }
  }, [province]);

    const onMunicipalityChange = (e) => {
    const municipalityValue = e.target.value;
    setMunicipality(municipalityValue);
    setBarangay('');
  }

    useEffect(() => {
    if (municipality === '') {
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const barangays = locationData[region].province_list[province].municipality_list[municipality].barangay_list.map((barangayName) => (
        <option key={barangayName} value={barangayName}>{barangayName}</option>
      ));
      setBarangayOptions(barangays);
      setIsActive(false);
    }
  }, [municipality]);

    useEffect(() => {
      if (email && password1 && password2 && name && username && region && province && municipality && barangay && completeAddress && password1 === password2) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, password1, password2, name, username, region, province, municipality, barangay, completeAddress]);




    // function to simulate user registration
function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: name,
            username: username,
            email: email,
            password: password1,
            region: region,
            province: province,
            municipality: municipality,
            barangay: barangay,
            completeAddress: completeAddress
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)
        if(data === true) {
            setName("")
            setUsername("")
            setEmail("");
            setPassword1("");
            setPassword2("");
            setRegion("")
            setProvince("");
            setMunicipality("");
            setBarangay("");
            navigate("/login")
            Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: `Thank you for registering ${name}!`
                })
        } else if (data.emailAlreadyExists) {
            alert("Email already in use")
        } else if (data.usernameAlreadyExists) {
            alert("Username already in use")
        } else if(data.inputError){
            alert("Please ensure all fields are complete")
        }
    });
}


    return (
        (user.id !== null)? 
        <Navigate to="/"/>
        :
     <>
     <Navbar className="fixed-top" bg="light" expand="lg" >
      <Container>
        <Navbar.Brand  href="/">
              <Image className="_logo1" src={mylogo} alt="National Book Shop" fluid/>
        </Navbar.Brand>
      </Container>
    </Navbar>
    <Container fluid >
          <Row>
               <Col>

                    <Form onSubmit={(e) => registerUser(e)} className="_registration_form mt-5 pt-5 px-2 w-50 mx-auto">

                      <h1 className="text-center pt-lg-5">Register</h1>
                      <Form.Group controlId="fullname">
                        <Form.Label >Fullname</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter fullname"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                          required
                        />
                      </Form.Group>

                      <Form.Group controlId="username">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter username"
                          value={username}
                          onChange={(e) => setUsername(e.target.value)}
                          required
                        />
                      </Form.Group>

                      <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          type="email"
                          placeholder="Enter email"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          required
                        />
                        <Form.Text className="text-muted">
                          We'll never share your email with anyone else.
                        </Form.Text>
                      </Form.Group>

                      <Form.Group controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          value={password1}
                          onChange={(e) => setPassword1(e.target.value)}
                          required
                        />
                      </Form.Group>

                      <Form.Group controlId="password2">
                        <Form.Label>Re-type Password</Form.Label>
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          value={password2}
                          onChange={(e) => setPassword2(e.target.value)}
                          required
                        />
                      </Form.Group>

                        <Form.Group controlId="region">
                          <Form.Label>Region</Form.Label>
                          <Form.Control as="select" value={region} onChange={(e) => setRegion(e.target.value)} required>
                            <option value="">-- Select Region --</option>
                            {regionOptions}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="province">
                          <Form.Label>Province</Form.Label>
                          <Form.Control as="select" value={province} onChange={(e) => setProvince(e.target.value)} required>
                            <option value="">-- Select Province --</option>
                            {provinceOptions}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="municipality">
                          <Form.Label>Municipality</Form.Label>
                          <Form.Control as="select" value={municipality} onChange={(e) => setMunicipality(e.target.value)} required>
                            <option value="">-- Select Municipality --</option>
                            {municipalityOptions}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="barangay">
                          <Form.Label>Barangay</Form.Label>
                          <Form.Control as="select" value={barangay} onChange={(e) => setBarangay(e.target.value)} required>
                            <option value="">-- Select Barangay --</option>
                            {barangayOptions}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="completeAddress">
                        <Form.Label>Complete Address</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter street,apartment or house no. "
                          value={completeAddress}
                          onChange={(e) => setCompleteAddress(e.target.value)}
                          required
                         />
                         </Form.Group>

                         <div className="d-flex justify-content-center mt-3 mb-5">
                           <Button className="" variant="danger" type="submit" disabled={!isActive}>
                             Register
                           </Button>
                         </div>

                      </Form>
               </Col>
               
               </Row>


    


  </Container>

  </>
      );
  


}
