/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import CartCard from "../components/CartCard";
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"

export default function Cart() {

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/myCart`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setOrders(data.map(order => {
				return(
					<CartCard key={order._id} order={order} />
				)
			}))
		})
	}, [])

	return (
		<>
		{orders}
		 <div>
          <Button variant="success" as={Link} to="/orders/checkOut" >Proceed to checkout</Button>
        </div>
		</>
	) 
}

