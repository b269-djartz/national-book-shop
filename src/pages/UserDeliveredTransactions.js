/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import CartCard from "../components/CartCard";
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import UserDeliveredTransactionsCard from "../components/UserDeliveredTransactionsCard";
import { NavLink } from 'react-router-dom';

export default function UserDeliveredTransactions() {

	const [userDeliveredTransactions, setUserDeliveredTransactions] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/transactions/delivered`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				console.log(data);
				setUserDeliveredTransactions(data.map(userDeliveredTransaction => {
				return(
					<UserDeliveredTransactionsCard key={userDeliveredTransaction._id} userDeliveredTransaction={userDeliveredTransaction} />
				)
			}))
			} else if (data.noTransactions) {
				return Swal.fire({
            	title: 'No Delivered Transactions!',
            	icon: 'warning',
            	text: 'Your product is still on the way',
          });
			} else {
				return console.log(data);
			}
			
		})
	}, [])


	return (
	  <>
	    <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
	      <NavLink to="/users/transactions">All Transactions</NavLink>
	      <NavLink to="/users/transactions/pending">Pending</NavLink>
	      <NavLink to="/users/transactions/shippedOut">Shipped Out</NavLink>
	      <NavLink to="/users/transactions/delivered">Delivered</NavLink>
	      <NavLink to="/users/transactions/cancelled">Cancelled</NavLink>
	    </div>
	    {userDeliveredTransactions}
	  </>
	);

}

