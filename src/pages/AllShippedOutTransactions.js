/*import coursesData from "../data/coursesData"*/
import {useState, useEffect,useContext} from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import AllShippedOutTransactionsCard from "../components/AllShippedOutTransactionsCard";
import { NavLink, useNavigate } from 'react-router-dom';

export default function AllShippedOutTransactions() {

	const [allShippedOutTransactions, setAllShippedOutTransactions] = useState([]);
	const navigate = useNavigate("");
	const [refresh, setRefresh] = useState(false);
	const {user} = useContext(UserContext)


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/transactions/shippedOut`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				console.log(data)
				setAllShippedOutTransactions(data.map(allShippedOutTransaction => {
				return(
					<>
					<AllShippedOutTransactionsCard
					        key={allShippedOutTransaction._id}
					        allShippedOutTransaction={allShippedOutTransaction}
					        deliverButton={deliverButton}
					      />


					</>

				)
			}))
			} else if (data.noTransactions) {
				return Swal.fire({
            	title: 'No ShippedOut Transactions!',
            	icon: 'warning'
          });
			} else {
				return console.log(data);
			}
			
		})
	}, [])

	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/transactions/shippedOut`, {
			  method: 'POST',
			  headers: {
			    'Content-Type': 'application/json',
			    Authorization: `Bearer ${localStorage.getItem('token')}`,
			  }
			})
			.then(res => res.json())
			.then(data => {
				if (data.length > 0) {
					setAllShippedOutTransactions(data.map(allShippedOutTransaction => {
					return(
						<>
						<AllShippedOutTransactionsCard
						        key={allShippedOutTransaction._id}
						        allShippedOutTransaction={allShippedOutTransaction}
						        deliverButton={deliverButton}
						      />


						</>

					)
				}))
				} else if (data.noTransactions) {
				return setAllShippedOutTransactions([])
			} else {
				return console.log(data);
			}
			
		})
	}, [refresh])


	function deliverButton(transactionId) {
    fetch(`${process.env.REACT_APP_API_URL}/transactions/shipToDelivered`, {
      method: 'PATCH',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              transactionId: transactionId,
            }),
          })
    .then(res => res.json())
    .then(data => {
      if(data===true) {
        Swal.fire({
                    	title: 'Success!',
                    	icon: 'success'
                  });
        setRefresh(!refresh);
        

      }

    })
  }


	return (
	  <>
	    <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
	      <NavLink to="/transactions/pending">Pending</NavLink>
	      <NavLink to="/transactions/shippedOut">Shipped Out</NavLink>
	      <NavLink to="/transactions/delivered">Delivered</NavLink>
	      <NavLink to="/transactions/cancelled">Cancelled</NavLink>
	    </div>
	    {allShippedOutTransactions.length > 0 ? allShippedOutTransactions : <p>No ShippedOut Transactions</p>}
	  </>
	);


}

