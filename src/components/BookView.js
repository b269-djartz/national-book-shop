import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, useNavigate, Link } from 'react-router-dom';

export default function BookView() {
  const {user} = useContext(UserContext);

  const navigate = useNavigate('');
  const { bookId } = useParams();

const [isBookInWishlist, setIsBookInWishlist] = useState(false);


const [isBookInBooksPurchased, setIsBookInBooksPurchased] = useState(false);
const [isBookReviewedByUser, setIsBookReviewedByUser] = useState(false);


  
  const [showReviewForm, setShowReviewForm] = useState(false);
  const [showReviewForm2, setShowReviewForm2] = useState(false);
  const [selectedStar, setSelectedStar] = useState(5);
  const [comment, setComment] = useState('Great Book!');
  
  const [title, setTitle] = useState('');
  const [genre, setGenre] = useState('');
  const [author, setAuthor] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [sold, setSold] = useState(0);
  const [starRating, setStarRating] = useState(0);
  const [ratings, setRatings] = useState(0);
  const [tags, setTags] = useState('');
  const [reviewButton, setReviewButton] = useState(true);
  const [reviews, setReviews] = useState([
    {
      username: '',
      name: '',
      star: 0,
      comment: '',
    },
  ]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(user.reviews);

        setTitle(data.title);
        setGenre(data.genre);
        setAuthor(data.author);
        setStock(data.stock);
        setDescription(data.description);
        setTags(data.tags);
        setPrice(data.price);
        setSold(data.sold);
        setReviews(data.reviews);
        setStarRating(data.starRating);
        setRatings(data.ratings);

      });
  }, [reviews]);


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkReview`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        bookId: bookId
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data)

       if(data === true) {
        setIsBookReviewedByUser(true)
       } else {
        setIsBookReviewedByUser(false)
       }

      });
  }, []);


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkBookPurchase`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        bookId: bookId
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data)
       if(data === true) {
        setIsBookInBooksPurchased(true)
       } else {
        setIsBookInBooksPurchased(false)
       }

      });
  }, []);


  const addToCart = (bookId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        bookId: bookId
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Added to cart',
          });
        } else if (data.adminRestricted) {
          Swal.fire({
            title: 'Oops',
            icon: 'error',
            text: 'Admins are not allowed to order',
          });
        } else if (data.requestExceedsStock) {
          Swal.fire({
            title: 'Requested quantity exceeds stock',
            icon: 'warning',
            text: 'Please check your cart',
          });
        }  
      });
  };

  // const isBookInWishlist = user && user.wishlist && user.wishlist.includes(title);
  const [showMoreReviews, setShowMoreReviews] = useState(false);


  const addToWishlist = () => {
  	fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/addToWishlist`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
    .then((res) => res.json())
    .then((data) => {
    	if(data === true) {
    		setIsBookInWishlist(true)
    		Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Book added to wishlist.',
          });
    	} else if (data.bookAlreadyExistsInWishlist){
    		Swal.fire({
            title: 'Duplicate',
            icon: 'info',
            text: 'Book already added to wishlist.',
          });
    		setIsBookInWishlist(true)
    	} else {
    		Swal.fire({
            title: 'User not found',
            icon: 'warning',
            text: 'Login First!',
          });
    	}
    })
}

  const handleSubmitReview = (e) => {
  e.preventDefault();

  const newReview = {
    username: user.username,
    name: user.name,
    star: selectedStar,
    comment: comment,
  };

  fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/addReview`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify(newReview),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'Review added.',
        });
        setReviews([...reviews, newReview]);
        setShowReviewForm(false)
        setReviewButton(false);
        setIsBookReviewedByUser(true)
      } else {
        console.log(data)
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Unable to add review.',
        });
      }
    });
};


const handleSubmitReview2 = (e) => {
  e.preventDefault();

  const newReview = {
    username: user.username,
    name: user.name,
    star: selectedStar,
    comment: comment,
  };

  fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/editReview`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify(newReview),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'Review Edited.',
        });
        const updatedReviews = reviews.map((review) =>
          review.username === user.username ? newReview : review
        );
        setReviews(updatedReviews);
        setShowReviewForm2(false);
      } else {
        console.log(data);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Unable to edit review.',
        });
      }
    });
};

const handleDeleteReview = (e,reviewIndex) => {
  e.preventDefault();


  fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/deleteReview`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    }
  })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'Review Deleted.',

        });
        setReviews(prevReviews => {
          const newReviews = [...prevReviews];
          newReviews.splice(reviewIndex, 1);
          return newReviews;
        });
        setIsBookReviewedByUser(false);
      } else {
        console.log(data);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Unable to delete review.',
        });
      }
    });
};




  const handleShowMoreReviews = () => {
    setShowMoreReviews(true);
  };

  const handleShowLessReviews = () => {
    setShowMoreReviews(false);
  };

  const reviewsToShow = showMoreReviews ? reviews : reviews.slice(0, 1);



  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Body className="text-center">

            <Row>
            <Col md={6}>
               <Card.Img className="w-50"variant='top' src={`https://picsum.photos/200?${title}-${author}`} />


              <Card.Title className="mt-2">{title}</Card.Title>
              <Card.Text >{genre} book</Card.Text >
              <Card.Subtitle>Author:</Card.Subtitle>
              <Card.Text >{author}</Card.Text>

               <div className="d-flex align-items-center justify-content-center">
                 <Card.Text className="_cardX"><strong>{starRating}</strong> |</Card.Text>
                 <Card.Text className="_cardX star-rating me-3" data-rating={starRating}></Card.Text>
                 <Card.Text className="_cardX me-3">ratings: <strong>{ratings}</strong></Card.Text>
                 <Card.Text >sold: <strong>{sold}</strong></Card.Text>
               </div>




            </Col>
            <Col md={6}>

              <Card.Subtitle className="mt-md-4">Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Text>&#127991;{tags && tags.join(',')}</Card.Text>
              <div className="d-flex align-items-center justify-content-center">
                <Card.Text className="_cardX me-md-5" >Price: <strong>&#8369; {price}</strong></Card.Text> 
                <Card.Text>Stock: <strong>{stock}</strong></Card.Text>
              </div>


              <div className="d-flex align-items-center justify-content-center mt-5">
              
              {
                (user.id !== null) ?
                  <Button variant="warning" onClick={() => addToCart(bookId)} ><strong>Add to cart</strong></Button>
                  :
                  <Button className="btn btn-danger" as={Link} to="/login"  >Login now to continue shoppping</Button>
              }
              <div>
              <Button className="ms-5"variant="secondary" onClick={addToWishlist} disabled={isBookInWishlist} >Add to Wishlist</Button>
              </div>
              </div>


            </Col>
            <Col md={12} className="w-50 mx-auto">


              <div>
                {
                  showReviewForm ? (
                  <>
                    <h5>Add a review</h5>
                    <Form onSubmit={handleSubmitReview}>
                      <Form.Group>
                        <Form.Label>Stars:</Form.Label>
                        <div>
                          {[1, 2, 3, 4, 5].map((star) => (
                            <Form.Check
                              key={star}
                              inline
                              label={star}
                              type="radio"
                              name="star"
                              value={star}
                              checked={star === selectedStar}
                              onChange={() => setSelectedStar(star)}
                            />
                          ))}
                        </div>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Comment:</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          placeholder="Enter your comment here..."
                          value={comment}
                          onChange={(e) => setComment(e.target.value)}
                        />
                      </Form.Group>
                      <Button variant="primary" type="submit">
                        Save
                      </Button>
                      <Button variant="primary" onClick={() => setShowReviewForm(false)}>
                        Cancel
                      </Button>
                    </Form>
                  </>
                ) : (
                    isBookInBooksPurchased && !isBookReviewedByUser ? (
                      <Button variant="success" onClick={() => setShowReviewForm(true)}>
                        Add a review
                      </Button>
                    ) : (

                      !isBookInBooksPurchased ?

                      <div className="text-muted"><em>Purchase this book to add a review</em></div> :
                      <div></div>
                    
                    )
                  
                )

              }
              </div>

              <Card.Subtitle>Reviews:</Card.Subtitle>
              {reviewsToShow.map((review, index) => (
                <>
                <Card key={index} className="mt-2">
                  <Card.Body>
                    <Card.Title>{review.name}</Card.Title>
                    <Card.Text>@{review.username}</Card.Text>
                    <Card.Text className="star-rating" data-rating={review.star}></Card.Text>
                    <Card.Subtitle>Comment:</Card.Subtitle>
                    <Card.Text>{review.comment}</Card.Text>
                  </Card.Body>
                  {isBookReviewedByUser && review.username === user.username ? (
                    <div>
                        {showReviewForm2 ? (
                          <>
                            <h5>Edit review</h5>
                            <Form onSubmit={handleSubmitReview2}>
                              <Form.Group>
                                <Form.Label>Stars:</Form.Label>
                                <div>
                                  {[1, 2, 3, 4, 5].map((star) => (
                                    <Form.Check
                                      key={star}
                                      inline
                                      label={star}
                                      type="radio"
                                      name="star"
                                      value={star}
                                      checked={star === selectedStar}
                                      onChange={() => setSelectedStar(star)}
                                    />
                                  ))}
                                </div>
                              </Form.Group>
                              <Form.Group>
                                <Form.Label>Comment:</Form.Label>
                                <Form.Control
                                  as="textarea"
                                  rows={3}
                                  placeholder="Enter your comment here..."
                                  value={comment}
                                  onChange={(e) => setComment(e.target.value)}
                                />
                              </Form.Group>
                              <Button variant="primary" type="submit">
                                Save
                              </Button>
                              <Button variant="primary" onClick={() => setShowReviewForm2(false)}>
                                Cancel
                              </Button>
                            </Form>
                          </>
                        ) : (
                        <>
                            <Button variant="success" onClick={() => setShowReviewForm2(true)}>
                                Edit review
                              </Button>
                              <Button variant="danger" onClick={handleDeleteReview}>
                                Delete review
                              </Button>

                        </>
                        )}
                    </div>

                    ) : <div></div> }
                </Card>
                </>
              ))}
              {reviews.length > 1 && (
                <div className="mt-2">
                  {!showMoreReviews ? (
                    <Button variant="link" onClick={handleShowMoreReviews}>
                      See more reviews
                    </Button>
                  ) : (
                    <Button variant="link" onClick={handleShowLessReviews}>
                      See less reviews
                    </Button>
                  )}
                </div>
            )}

            </Col>
            </Row>

             
              

            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
