import { Button, Row, Col, Carousel, Image} from "react-bootstrap";
import pup from '../images/pup.png';
import photo3 from '../images/photo3.jpg';
import photo2 from '../images/photo2.png';
import fantasy from '../images/fantasy.jpg';

export default function Banner() {
  return (
    <Row>
      <Col className=" col-12 d-flex justify-content-center align-items-center">
        <Carousel fade>
          <Carousel.Item className = "d-flex justify-content-center">
            <img
              className="d-block img-fluid "
              src={photo2}
              alt="First slide"
            />
          </Carousel.Item >
          <Carousel.Item className = "d-flex justify-content-center">
            <Image
              className="d-block img-fluid"
              src={photo3}
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item className = "d-flex justify-content-center">
            <Image
              className="d-block img-fluid"
              src={fantasy}
              alt="Third slide"
            />
          </Carousel.Item>
        </Carousel>
      </Col>
    </Row>
  );
}
